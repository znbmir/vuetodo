new Vue({
  el: '#app',
  data: {
    todoText:'',
    todos: [],
  },
  methods: {
    removeTodo: function(index){
      this.todos.splice(index, 1);
    },
    addTodo: function(){
      var todoText = this.todoText.trim();
      if(todoText){
        this.todos.push({title: todoText});
        this.todoText = '';
      }
        }
  }
});
